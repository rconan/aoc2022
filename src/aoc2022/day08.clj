(ns aoc2022.day08
  (:require
    [aoc2022.utils :as u])
  (:gen-class))


(defn parse [lines]
  (map #(map (fn [^Character c] (Character/digit c 10)) %) lines))

(defn visible-left [f i j]
  (let [row (nth f j)]
    (every? identity (map #(< % (nth row i)) (take i row)))))

(defn visible-right [f i j]
  (let [row (nth f j)]
    (every? identity (map #(< % (nth row i)) (drop (inc i) row)))))

(defn visible-top [f i j]
  (let [column (map #(nth % i) f)]
    (every? identity (map #(< % (nth column j)) (take j column)))))

(defn visible-bottom [f i j]
  (let [column (map #(nth % i) f)]
    (every? identity (map #(< % (nth column j)) (drop (inc j) column)))))

(defn visible [f i j]
  (or
    (visible-top f i j)
    (visible-bottom f i j)
    (visible-left f i j)
    (visible-right f i j)))

(defn vision-score [line height]
  (loop [l line score 0]
    (cond
      (empty? l) score
      (>= (first l) height) (inc score)
      :else (recur (rest l) (inc score)))))

(defn distance-left [f i j]
  (let [row (nth f j)]
    (vision-score (reverse (take i row)) (nth row i))))

(defn distance-right [f i j]
  (let [row (nth f j)]
    (vision-score (drop (inc i) row) (nth row i))))

(defn distance-top [f i j]
  (let [column (map #(nth % i) f)]
    (vision-score (reverse (take j column)) (nth column j))))

(defn distance-bottom [f i j]
  (let [column (map #(nth % i) f)]
    (vision-score (drop (inc j) column) (nth column j))))

(defn distance [f i j]
  (*
    (distance-top f i j)
    (distance-bottom f i j)
    (distance-left f i j)
    (distance-right f i j)))

(defn count-visible-row [f j]
  (let [max-i (dec (count (nth f j)))]
    (loop [total 0 i 1]
      (if (= i max-i)
        total
        (recur (if (visible f i j) (inc total) total) (inc i)))))
    )

(defn count-visible [f]
  (+ -4
     (* 4 (count f))
     (let [max-j (dec (count f))]
       (loop [total 0 j 1]
         (if (= j max-j)
           total
           (recur (+ total (count-visible-row f j)) (inc j))))))
       )

(defn max-distance-row [f j]
  (let [max-i (dec (count (nth f j)))]
    (loop [m 0 i 1]
      (if (= i max-i)
        m
        (recur (max m (distance f i j)) (inc i)))))
    )

(defn max-distance [f]
  (let [max-j (dec (count f))]
    (loop [m 0 j 1]
      (if (= j max-j)
        m
        (recur (max m (max-distance-row f j)) (inc j)))))
    )

(defn part1 [p]
  (println (count-visible (parse (u/input-lines p)))))


(defn part2 [p]
  (println (max-distance (parse (u/input-lines p)))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))
