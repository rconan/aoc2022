(ns aoc2022.day12
  (:require
    [clojure.data.priority-map :refer [priority-map]]
    [aoc2022.utils :as u])
  (:gen-class))

(defn char-height [c]
  (case c
    \S 1
    \E 26
    (- (int c) 96)))

(defn parse-line [m j line]
  (loop [l line i 0 m m]
    (if (empty? l)
      m (recur
          (rest l)
          (inc i)
          {:grid (assoc-in (:grid m) [j i] (char-height (first l)))
           :start (if (= (first l) \S) [i j] (:start m))
           :end (if (= (first l) \E) [i j] (:end m))
           }))))

(defn parse [lines]
  (loop [l lines j 0 m {:grid (vec (repeat (count lines) (vec (repeat 0 (count (first lines)))))) :start nil :end nil}]
    (if (empty? l)
      m (recur
          (rest l)
          (inc j)
          (parse-line m j (first l)))))
  )

(defn candidates [grid i j comp]
  (loop [options [[1 0] [-1 0] [0 1] [0 -1]]
         result '()]
    (if (empty? options)
      result
      (recur (rest options)
             (let [ii (+ i (first (first options)))
                   jj (+ j (second (first options)))]
               (if (and (< -1 jj (count grid))
                        (< -1 ii (count (nth grid jj)))
                        (comp (get-in grid [jj ii]) (get-in grid [j i])))
                 (cons [ii jj] result) result))))))

(defn graph [grid comp]
  (loop [j 0 graph {}]
    (if (= j (count grid))
      graph
      (recur
        (inc j)
        (loop [i 0 graph graph]
          (if (= i (count (nth grid j)))
            graph
            (recur
              (inc i)
              (assoc graph [i j] (candidates grid i j comp))))))
      ))
  )

(defn distance [from to]
  (reduce + (map #(max (- %) %) (map - from to))))

(defn get-or-inf [map key]
  (if (contains? map key)
    (get map key)
    Integer/MAX_VALUE
    ))

(defn explore [state current neighbour h]
  (let [gScore (:gScore state)
        tentativeGScore (inc (get-or-inf gScore current))]
    (if (< tentativeGScore (get-or-inf gScore neighbour))
      {:cameFrom (assoc (:cameFrom state) neighbour current)
       :gScore (assoc gScore neighbour tentativeGScore)
       :openSet (assoc (:openSet state) neighbour (+ tentativeGScore (h neighbour)))
       }
      state
      )))

(defn reconstruct-path [cameFrom end]
  (loop [path [end] current end]
    (if (contains? cameFrom current)
      (let [new (get cameFrom current)]
        (recur (cons new path) new))
      path)))

(defn a* [graph start done? h]
  (loop [state {:openSet (priority-map start (h start)) :cameFrom {} :gScore {start 0}}]
    (let [current (first (peek (:openSet state)))]
      (if (done? current)
        (reconstruct-path (:cameFrom state) current)
        (recur (loop [neighbours (get graph current) s (assoc state :openSet (pop (:openSet state)))]
                 (if (empty? neighbours)
                   s
                   (recur (rest neighbours) (explore s current (first neighbours) h)))))))))

(defn part1 [p]
  (as-> (u/input-lines p) v
        (parse v)
        (assoc v :graph (graph (:grid v) (fn [lhs rhs] (<= lhs (inc rhs)))))
        (a* (:graph v) (:start v) #(= % (:end v)) #(distance % (:end v)))
        (dec (count v))
        (println v)))

(defn part2 [p]
  (as-> (u/input-lines p) v
        (parse v)
        (assoc v :graph (graph (:grid v) (fn [lhs rhs] (>= lhs (dec rhs)))))
        (a* (:graph v) (:end v) #(= (get-in (:grid v) [(second %) (first %)]) 1) (constantly 1))
        (dec (count v))
        (println v)))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))
