(ns aoc2022.day02
  (:require
    [aoc2022.utils :as u])
  (:gen-class))


(defn move-score [c]
  (cond
    (or (= c \A) (= c \X))  1
    (or (= c \B) (= c \Y))  2
    (or (= c \C) (= c \Z))  3))

(defn beat-move [c]
  (cond
    (= c \A) \B
    (= c \B) \C
    (= c \C) \A))

(defn lose-move [c]
  (cond
    (= c \A) \C
    (= c \B) \A
    (= c \C) \B))

(defn add-mod-3 [n i]
  (mod (+ n i) 3))

(defn desired-score [c]
  (cond
    (= c \X) 0
    (= c \Y) 3
    (= c \Z) 6))

(defn result-score [o y]
  (cond
    (= (mod o 3) (add-mod-3 y 1)) 0
    (= o y) 3
    (= (mod o 3) (add-mod-3 y 2)) 6))

(defn desired-move-score [o y]
  (cond
    (= y \X) (move-score (lose-move o))
    (= y \Y) (move-score o)
    (= y \Z) (move-score (beat-move o))))

(defn game-score [o y]
  (+
    (result-score (move-score o) (move-score y))
    (move-score y)))

(defn desired-game-score [o y]
  (+
    (desired-score y)
    (desired-move-score o y)))

(defn line-score [l]
  (game-score (nth l 0) (nth l 2)))

(defn desired-line-score [l]
  (desired-game-score (nth l 0) (nth l 2)))

(defn part1 [p]
  (let [lines (u/input-lines p)]
    (println (reduce + (map line-score (filter #(not= "" %) lines))))))

(defn part2 [p]
  (let [lines (u/input-lines p)]
    (println (reduce + (map desired-line-score (filter #(not= "" %) lines))))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))