(ns aoc2022.day11
  (:require
    [tupelo.string :as str]
    [aoc2022.utils :as u])
  (:gen-class))

(defn operand [s]
  (if (= s "old") :old (Integer/parseInt s)))

(defn parse [lines]
  (let [operation (drop 4 (str/split (nth lines 2) #"\s+"))]
    {
     :items (vec (map #(Integer/parseInt %) (drop 3 (str/split (nth lines 1) #"[\s,]+"))))
     :operation {:operator ({"+" + "*" *} (nth operation 1))
                 :lhs (operand (nth operation 0))
                 :rhs (operand (nth operation 2))
                 }
     :test {
            :divisor (Integer/parseInt (nth (str/split (nth lines 3) #"\s+") 4))
            :pass (Integer/parseInt (nth (str/split (nth lines 4) #"\s+") 6))
            :fail (Integer/parseInt (nth (str/split (nth lines 5) #"\s+") 6))
            }
     :count 0
     }))

(defn monkey-item [monkey item monkeys d mo]
  (let [operation (:operation monkey)
        lhs (:lhs operation)
        rhs (:rhs operation)
        investigated ((:operator operation) (if (= lhs :old) item lhs) (if (= rhs :old) item rhs))
        w (bigint (/ investigated d))
        worry (if (nil? mo) w (mod w mo))
        test (:test monkey)
        target (if (zero? (mod worry (:divisor test))) (:pass test) (:fail test))
        target-monkey (nth monkeys target)]
    (assoc monkeys target (assoc target-monkey :items (conj (:items target-monkey) worry)))))

(defn monkey [i m d mo]
  (let [monkey (nth m i)]
    (assoc
    (loop [monkeys m
           items (:items monkey)]
      (if (empty? items)
        monkeys
        (recur (monkey-item monkey (first items) monkeys d mo) (rest items)))
      ) i (assoc monkey :count (+ (:count monkey) (count (:items monkey))) :items []))
    )
  )

(defn round [m d mo]
  (let [n (count m)]
    (loop [i 0 monkeys m]
      (if (= i n) monkeys (recur (inc i) (monkey i monkeys d mo))))))

(defn play [n d mo m]
  (loop [i 0 monkeys m]
    (if (= i n) monkeys (recur (inc i) (round monkeys d mo)))))

(defn monkey-business [monkeys]
  (apply * (take-last 2 (sort (map :count monkeys)))))

(defn part1 [p]
  (->> (u/input-lines p)
       (u/split-by-empty-line)
       (map parse)
       (vec)
       (play 20 3 nil)
       (monkey-business)
       (println)))

(defn part2 [p]
  (let [monkeys (->> (u/input-lines p)
                     (u/split-by-empty-line)
                     (map parse)
                     (vec))]
    (->>
      monkeys
      (play 10000 1 (apply u/lcm (map #(:divisor (:test %)) monkeys)))
      (monkey-business)
      (println))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))
