(ns aoc2022.day04
  (:require
    [tupelo.string :as str]
    [aoc2022.utils :as u])
  (:gen-class))


(defn parse [s]
  (map #(Integer/parseInt %) (str/split s #"[,-]")))

(defn fully-contained [l]
  (or
    (<= (nth l 0) (nth l 2) (nth l 3) (nth l 1))
    (<= (nth l 2) (nth l 0) (nth l 1) (nth l 3))))

(defn overlap [l]
  (or
    (and
      (<= (nth l 2) (nth l 1))
      (>= (nth l 3) (nth l 0)))
    (and
      (<= (nth l 0) (nth l 3))
      (>= (nth l 1) (nth l 2)))))

(defn part1 [p]
  (let [lines (u/input-lines p)]
    (println (count (filter identity (map fully-contained (map parse lines)))))))

(defn part2 [p]
  (let [lines (u/input-lines p)]
    (println (count (filter identity (map overlap (map parse lines)))))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))