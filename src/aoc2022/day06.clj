(ns aoc2022.day06
  (:require
    [aoc2022.utils :as u])
  (:gen-class))


(defn marker [l k]
  (let [bsize (- k 1)]
    (loop [b (take bsize l) t (drop bsize l) n k]
      (let [s (set b)]
        (if (and
                (= (count s) bsize)
                (not (s (first t)))) n
        (recur (concat (rest b) [(first t)]) (rest t) (inc n)))))))

(defn part1 [p]
  (let [lines (u/input-lines p)]
    (doseq [r (map #(marker % 4) lines)] (println r))))

(defn part2 [p]
  (let [lines (u/input-lines p)]
    (doseq [r (map #(marker % 14) lines)] (println r))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))
