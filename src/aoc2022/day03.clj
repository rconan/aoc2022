(ns aoc2022.day03
  (:require
    [aoc2022.utils :as u])
  (:gen-class))


(defn priority [c]
  (let [i (int c)]
    (cond
      (<= 97 i 122) (- i 96)
      (<= 65 i 90) (- i 38))))

(defn duplicate [p]
  (first (filter #(contains? (set (first p)) %) (second p))))

(defn three-match [p]
  (first
    (filter
      #(and
        (contains? (set (first p)) %)
        (contains? (set (second p)) %))
      (nth p 2))))

(defn bisect [s]
  (partition (/ (count s) 2) s))

(defn part1 [p]
  (let [lines (u/input-lines p)]
    (println (reduce + (map priority (map duplicate (map bisect lines)))))))

(defn part2 [p]
  (let [lines (u/input-lines p)]
    (println (reduce + (map priority (map three-match (partition 3 lines)))))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))