(ns aoc2022.day13
  (:require
    [instaparse.core :as insta]
    [aoc2022.utils :as u])
  (:gen-class))

(defn packet [s]
  (insta/transform {:number #(Integer/parseInt %) :list list} ((insta/parser
    "<S> = expr
    <expr> = number | list
    number = #'[0-9]+'
    list = <'['> (expr (<','> expr)*)? <']'>") s)))

(defn compare-packets [a b]
  (cond
    (and (number? a) (number? b)) (compare a b)
    (number? a) (recur [a] b)
    (number? b) (recur a [b])
    (and (empty? a) (empty? b)) 0
    (empty? a) -1
    (empty? b) 1
    :else (let [c (compare-packets (first a) (first b))]
            (if (not= c 0) c (recur (rest a) (rest b))))))

(defn part1 [p]
  (->> (u/input-lines p)
       (u/split-by-empty-line)
       (map #(map packet %))
       (map #(apply compare-packets %))
       (keep-indexed #(if (= %2 -1) %1 nil))
       (map inc)
       (reduce +)
       (println)))

(defn part2 [p]
  (->> (u/input-lines p)
       (filter #(not= "" %))
       (map packet)
       (cons [[2]])
       (cons [[6]])
       (sort compare-packets)
       (keep-indexed #(if (#{[[2]] [[6]]} %2) %1 nil))
       (map inc)
       (reduce *)
       (println)))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))