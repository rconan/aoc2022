(ns aoc2022.day16
  (:require
    [clojure.set :as set]
    [instaparse.core :as insta]
    [aoc2022.utils :as u])
  (:gen-class))

(defn valve [s]
  (insta/transform
    {:number #(Integer/parseInt %)
     :valves list}
    ((insta/parser
       "<S> = <'Valve '> valve <' has flow rate='> number <#'; tunnels? leads? to valves? '> valves
       valves = valve (<', '> valve)*
       <valve> = #'[A-Z]{2}'
       number = #'-?[0-9]+'") s)))

(defn flows [valves]
  (loop [fs {} vs valves]
    (if (empty? vs)
      fs
      (recur
        (let [valve (first vs)
              flow (second valve)]
          (if (zero? flow)
            fs (assoc fs (first valve) flow)))
        (rest vs)))))

(defn tunnels [valves]
  (loop [ps {} vs valves]
    (if (empty? vs)
      ps
      (let [v (first vs)
            valve (first v)]
        (recur (loop [ps ps tunnels (nth v 2)]
                 (if (empty? tunnels)
                   ps
                   (recur (assoc ps [valve (first tunnels)] 1)
                          (rest tunnels))))
               (rest vs))))))

(defn all-paths [paths names]
  (loop [all paths ks names]
    (if (empty? ks)
      all
      (recur (loop [all all is names]
               (if (empty? is)
                 all
                 (recur (loop [all all js names]
                          (if (empty? js)
                            all
                            (recur
                              (let [k (first ks)
                                    i (first is)
                                    j (first js)
                                    c (+ (get all [i k] Integer/MAX_VALUE)
                                         (get all [k j] Integer/MAX_VALUE))]
                                (if (< c (get all [i j] Integer/MAX_VALUE))
                                  (assoc all [i j] c)
                                  all))
                              (rest js))))
                        (rest is))))
             (rest ks))))
  )

(defn paths [valves]
  (let [names (map first valves)]
    (as-> valves v
        (tunnels v)
        (all-paths v names)))
    )

(defn process [valves]
  {:flows (flows valves)
   :paths (paths valves)
   })

(defn pressures [paths flows current remaining visited released]
  (if (or (empty? flows) (<= remaining 0))
    {visited released} (reduce
         (partial merge-with max)
         {visited released}
         (map
           #(let [remain (dec (- remaining (get paths [current %])))]
              (pressures
                 paths
                 (dissoc flows %)
                 %
                 remain
                 (conj visited %)
                 (+ released (* remain (get flows %)))) )
             (keys flows)))))

(defn two-pressure [paths flows start time]
  (let [pressures (pressures paths flows start time #{} 0)]
    (loop [best 0 ones pressures]
      (if (empty? ones)
        best
        (recur
          (loop [best best twos (rest ones)]
            (if (empty? twos)
              best
              (recur
                (if
                  (empty? (set/intersection (first (first ones)) (first (first twos))))
                  (max best (+ (second (first ones)) (second (first twos))))
                  best)
                (rest twos))))
          (rest ones)))))
  )

(defn part1 [p]
  (as-> p v
        (pressures (:paths v) (:flows v) "AA" 30 [] 0)
        (vals v)
        (reduce max v)
        (println v)))

(defn part2 [p]
  (as-> p v
        (two-pressure (:paths v) (:flows v) "AA" 26)
        (println v)))

(defn -main [& args]
  (let [processed (->> (first args)
                       (u/input-lines)
                       (map valve)
                       (process))]
    (part1 processed)
    (part2 processed)))
