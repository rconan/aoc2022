(ns aoc2022.day01
  (:require
    [tupelo.core :as t]
    [aoc2022.utils :as u])
  (:gen-class))


(defn parse [l]
  (map #(Integer/parseInt %) l))

(defn trim-and-parse [l]
  (if (= (first l) "")
    (parse (drop 1 l))
    (parse l)))

(defn sum [l]
  (reduce + l))

(defn sets [lines]
  (map trim-and-parse (t/partition-using u/first-is-empty-string lines)))

(defn part1 [p]
  (let [lines (u/input-lines p)]
    (println (reduce max (map sum (sets lines))))))

(defn part2 [p]
  (let [lines (u/input-lines p)]
    (println (sum (take 3 (sort > (map sum (sets lines))))))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))