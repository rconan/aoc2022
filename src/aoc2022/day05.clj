(ns aoc2022.day05
  (:require
    [tupelo.core :as t]
    [tupelo.string :as str]
    [aoc2022.utils :as u])
  (:gen-class))


(defn parse-stacks-inner [lines n]
  (cond
    (= n 0) []
    :else (cons (remove #(= \  %) (map first lines)) (parse-stacks-inner (map #(drop 4 %) lines) (- n 1)))))

(defn parse-stacks [lines]
  (vec (parse-stacks-inner (map #(drop 1 %) (drop-last lines))  (Integer/parseInt (last (str/split (last lines) #"\s+"))))))

(defn parse-move [line]
  (let [tokens (str/split line #"\s+")]
    {:count (Integer/parseInt (nth tokens 1))
     :from (- (Integer/parseInt (nth tokens 3)) 1)
     :to (- (Integer/parseInt (nth tokens 5)) 1)}))

(defn parse-moves [lines]
  (map parse-move lines))

(defn apply-move [stacks move mutator]
  (assoc
    (assoc
      stacks
      (:from move)
      (drop (:count move) (nth stacks (:from move))))
    (:to move)
    (concat (mutator (take (:count move) (nth stacks (:from move)))) (nth stacks (:to move)))))

(defn apply-moves [stacks moves mutator]
  (cond
    (empty? moves) stacks
    :else (apply-moves (apply-move stacks (first moves) mutator) (rest moves) mutator)))

(defn part1 [p]
  (let [parts (t/partition-using u/first-is-empty-string (u/input-lines p))
        stacks (parse-stacks (first parts))
        moves (parse-moves (drop 1 (second parts)))]
    (println (apply str (map first (apply-moves stacks moves reverse))))))

(defn part2 [p]
  (let [parts (t/partition-using u/first-is-empty-string (u/input-lines p))
        stacks (parse-stacks (first parts))
        moves (parse-moves (drop 1 (second parts)))]
    (println (apply str (map first (apply-moves stacks moves identity))))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))
