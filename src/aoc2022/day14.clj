(ns aoc2022.day14
  (:require
    [instaparse.core :as insta]
    [tupelo.core]
    [aoc2022.utils :as u])
  (:gen-class))

(defn path [s]
  (insta/transform
    {:number #(Integer/parseInt %)
     :pos list}
    ((insta/parser
       "<S> = pos (<' -> '> pos)+
       pos = number <','> number
       number = #'[0-9]+'") s)))

(defn populate-path [grid path]
  (let [start (first path)
        end (second path)
        v (map #(compare 0 %) (map - start end))
        ]
    (loop [grid grid pos start]
      (if (= pos end)
        (conj grid pos)
        (recur (conj grid pos) (map + pos v))))))

(defn populate [grid paths]
  (reduce populate-path grid paths))

(defn simulate-abyss [grid lowest]
  (loop [sand [500 0]]
    (if (= (second sand) lowest)
      nil
      (let [new (some #(if (grid %) nil %) (map #(map + sand %) [[0 1] [-1 1] [1 1]]))]
        (if (= new nil)
          sand
          (recur new))))))

(defn simulate-abyss [grid lowest]
  (loop [sand [500 0]]
    (if (= (second sand) lowest)
      nil
      (let [new (some #(if (grid %) nil %) (map #(map + sand %) [[0 1] [-1 1] [1 1]]))]
        (if (= new nil)
          sand
          (recur new))))))

(defn simulate-floor [grid lowest]
  (let [floor (inc lowest)]
    (loop [sand [500 0]]
      (if (= (second sand) floor)
        sand
        (let [new (some #(if (grid %) nil %) (map #(map + sand %) [[0 1] [-1 1] [1 1]]))]
          (if (= new nil)
            sand
            (recur new)))))))

(defn simulate [tick done grid]
  (let [n (count grid)
        lowest (reduce #(max %1 (second %2)) 0 grid)]
    (loop [grid grid]
      (let [new (tick grid lowest)]
        (if (done new)
          (- (count grid) n)
          (recur (conj grid new)))))))

(defn part1 [p]
  (->> (u/input-lines p)
       (map path)
       (map u/pairwise)
       (reduce populate #{})
       (simulate simulate-abyss nil?)
       (println)))

(defn part2 [p]
  (->> (u/input-lines p)
       (map path)
       (map u/pairwise)
       (reduce populate #{})
       (simulate simulate-floor #(= [500 0] %))
       (inc)
       (println)))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))
