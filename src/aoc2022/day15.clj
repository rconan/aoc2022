(ns aoc2022.day15
  (:require
    [instaparse.core :as insta]
    [tupelo.core :as t]
    [aoc2022.utils :as u])
  (:gen-class))

(defn beacon [s]
  (insta/transform
    {:number #(Integer/parseInt %)
     :coords list}
    ((insta/parser
       "<S> = <'Sensor at '> coords <': closest beacon is at '> coords
       coords = <'x='> number <', y='> number
       number = #'-?[0-9]+'") s)))

(defn manhattan-distance [a b]
  (reduce + (map #(max % (- %)) (map - a b))))

(defn x-range [sensor beacon y]
  (let [distance (manhattan-distance sensor beacon)
        sensor-x (first sensor)
        diff-y (u/abs (- (second sensor) y))
        row-width (- distance diff-y)]
    (if (neg? row-width)
      nil
      [(- sensor-x row-width) (+ sensor-x row-width)])))

(defn super-range [coll]
  (loop [lower Integer/MAX_VALUE upper Integer/MIN_VALUE coll coll]
    (if (empty? coll)
      [lower upper]
      (recur (min lower (first (first coll))) (max upper (second (first coll))) (rest coll)))))

(defn blocked [range coll]
  (loop [curr (first range) acc 0]
    (if (> curr (second range))
      acc
      (recur (inc curr)
             (loop [coll coll]
               (cond
                 (empty? coll) acc
                 (<= (first (first coll)) curr (second (first coll))) (inc acc)
                 :else (recur (rest coll))))))))

(defn find-hole [coll lower upper]
  (let [coll (sort #(compare (first %1) (first %2)) (filter #(and (>= (second %) lower) (<= (first %) upper)) coll))]
    (loop [range (first coll) coll (rest coll)]
      (cond
        (empty? coll) nil
        (= (inc (second range)) (dec (first (first coll)))) (inc (second range))
        :else (recur [(first range) (max (second range) (second (first coll)))] (rest coll))))))

(defn part1 [p row]
  (let [beacons (map beacon (u/input-lines p))
        ranges (filter t/not-nil? (map #(x-range (first %) (second %) row) beacons))]
    (println (- (blocked (super-range ranges) ranges)
                (count (set (filter #(= (second %) row) (map second beacons))))))))

(defn part2 [p upper]
  (let [beacons (map beacon (u/input-lines p))]
    (loop [row 0]
      (let [r (find-hole (filter t/not-nil? (map #(x-range (first %) (second %) row) beacons)) 0 upper)]
        (cond
          (t/not-nil? r) (println (+ (* r 4000000) row))
          (not= row upper) (recur (inc row)))))))

(defn -main [& args]
  (part1 (first args) 10)
  (part2 (first args) 4000000))
