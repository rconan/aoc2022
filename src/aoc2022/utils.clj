(ns aoc2022.utils
  (:require [clojure.string :as str]
            [clojure.math.numeric-tower :as math]
            [tupelo.core :as t]))

(defn abs [n]
  (max n (- n)))

(defn input-lines [f]
  (str/split-lines (slurp f)))

(defn first-is-empty-string [l]
  (= (first l) ""))

(defn split-by-empty-line [l]
  (map #(if (= (first %) "") (rest %) %) (t/partition-using first-is-empty-string l)))

(defn lcm [& n]
  (loop [r 1 numbers n]
    (if (empty? numbers)
      r (recur (math/lcm r (first numbers)) (rest numbers)))))

(defn pairwise [coll]
  (loop [l (first coll) r (rest coll) pairs []]
    (if (empty? r)
      pairs
      (recur (first r) (rest r) (conj pairs [l (first r)])))))

(defn parse-char [lines]
  ({
    [[\. \# \# \.] [\# \. \. \#] [\# \. \. \#] [\# \# \# \#] [\# \. \. \#] [\# \. \. \#]] \A
    [[\# \# \# \.] [\# \. \. \#] [\# \# \# \.] [\# \. \. \#] [\# \. \. \#] [\# \# \# \.]] \B
    [[\. \# \# \.] [\# \. \. \#] [\# \. \. \.] [\# \. \. \.] [\# \. \. \#] [\. \# \# \.]] \C
    [[\# \# \# \#] [\# \. \. \.] [\# \# \# \.] [\# \. \. \.] [\# \. \. \.] [\# \# \# \#]] \E
    [[\# \# \# \#] [\# \. \. \.] [\# \# \# \.] [\# \. \. \.] [\# \. \. \.] [\# \. \. \.]] \F
    [[\. \# \# \.] [\# \. \. \#] [\# \. \. \.] [\# \. \# \#] [\# \. \. \#] [\. \# \# \#]] \G
    [[\# \. \. \#] [\# \. \. \#] [\# \# \# \#] [\# \. \. \#] [\# \. \. \#] [\# \. \. \#]] \H
    [[\. \. \# \#] [\. \. \. \#] [\. \. \. \#] [\. \. \. \#] [\# \. \. \#] [\. \# \# \.]] \J
    [[\# \. \. \.] [\# \. \. \.] [\# \. \. \.] [\# \. \. \.] [\# \. \. \.] [\# \# \# \#]] \L
    [[\# \# \# \.] [\# \. \. \#] [\# \. \. \#] [\# \# \# \.] [\# \. \. \.] [\# \. \. \.]] \P
    [[\# \# \# \.] [\# \. \. \#] [\# \. \. \#] [\# \# \# \.] [\# \. \# \.] [\# \. \. \#]] \R
    [[\# \. \. \#] [\# \. \. \#] [\# \. \. \#] [\# \. \. \#] [\# \. \. \#] [\. \# \# \.]] \U
    [[\# \# \# \#] [\. \. \. \#] [\. \. \# \.] [\. \# \. \.] [\# \. \. \.] [\# \# \# \#]] \Z
    } lines))

(defn parse-text [lines]
  (str/join
    (reverse
      (loop [l lines s ""]
        (if
          (some empty? l)
          s
          (recur (map #(drop 5 %) l) (cons (parse-char (map #(take 4 %) l)) s)))))))