(ns aoc2022.day07
  (:require
    [tupelo.core :as t]
    [tupelo.string :as str]
    [aoc2022.utils :as u])
  (:gen-class))


(defn du [cmds]
  nil)

(defn parse-command [cmd]
  (let [tokens (str/split cmd #"\s+")]
    {:cmd ({"cd" :cd "ls" :ls} (second tokens))
     :arg (get tokens 2)}))

(defn parse-entry [entry]
  (let [tokens (str/split entry #"\s+")]
    (cond
      (= (first tokens) "dir") {:type :dir :name (second tokens)}
      :else {:type :file
             :name (second tokens)
             :size (Integer/parseInt (first tokens))})))

(defn parse [lines]
  {:command (parse-command (first lines))
   :result (map parse-entry (rest lines))})

(defn update-tree [tree pwd entries]
  (case (count entries)
    0 tree
    (recur
      (assoc-in tree (reverse (cons (:name (first entries)) pwd)) (:size (first entries)))
      pwd
      (rest entries))))

(defn process-command [state command]
  (case (:cmd (:command command))
    :cd {:tree (:tree state)
         :pwd (case (:arg (:command command))
                "/" []
                ".." (drop 1 (:pwd state))
                (cons (:arg (:command command)) (:pwd state)))}
    :ls {:tree (update-tree (:tree state) (:pwd state) (filter #(= (:type %) :file) (:result command)))
         :pwd (:pwd state)}))

(defn build-tree [p]
  (->> (u/input-lines p)
       (t/partition-using #(= (first (first %)) \$))
       (map parse)
       (reduce process-command {:tree {} :pwd []})
       (:tree)))

(defn dir-sizes [tree]
  (let [dir-results (map dir-sizes (filter map? (vals tree)))
        tops (map first dir-results)
        children (flatten (map rest dir-results))]
    [(+
       (reduce + (filter number? (vals tree)))
       (reduce + tops))
     (concat tops children)]))

(defn part1 [p]
  (println (reduce + (filter #(<= % 100000) (flatten (dir-sizes (build-tree p)))))))

(defn part2 [p]
  (let [[top children] (dir-sizes (build-tree p))
        target (- top 40000000)
        all (cons top children)]
    (println (reduce min (filter #(>= % target) all)))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))
