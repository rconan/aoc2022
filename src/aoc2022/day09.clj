(ns aoc2022.day09
  (:require
    [tupelo.string :as str]
    [aoc2022.utils :as u])
  (:gen-class))


(defn parse-line [l]
  (let [tokens (str/split l #"\s+")]
    {:direction ({"U" :up "D" :down "L" :left "R" :right} (first tokens))
     :distance (Integer/parseInt (second tokens))}))

(defn reconcile-tail [head tail]
  (let [diff (map - head tail)]
    (if (> (apply max (map #(max % (- %)) diff)) 1)
      (map + tail (map #(compare % 0) diff))
      tail)))

(defn reconcile-knots [head knots]
  (rest (reductions reconcile-tail head knots)))

(defn process-direction [state dir]
  (let [head (map + (:head state) (dir {:up '(0 -1) :down '(0 1) :left '(-1 0) :right '(1 0)}))
        tail (reconcile-knots head (:tail state))]
  {:head head
     :tail tail
     :visited (conj (:visited state) (last tail))}))

(defn process-instruction [state inst]
  (loop [i 0 s state]
    (if (= i (:distance inst)) s
      (recur (inc i) (process-direction s (:direction inst))))))

(defn part1 [p]
  (println (count (:visited (reduce process-instruction {:head '(0 0) :tail ['(0 0)]  :visited #{}} (map parse-line (u/input-lines p)))))))

(defn part2 [p]
  (println (count (:visited (reduce process-instruction {:head '(0 0) :tail (repeat 9 '(0 0)) :visited #{}} (map parse-line (u/input-lines p)))))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))
