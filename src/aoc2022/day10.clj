(ns aoc2022.day10
  (:require
    [tupelo.string :as str]
    [aoc2022.utils :as u])
  (:gen-class))

(defn tick [state]
  (let [program (:program state)
        cmd (first program)
        inst (:inst cmd)

        clock (inc (:clock state))
        cycles (inc (:cycles state))
        done (= cycles ({:noop 1 :addx 2} inst))
        x (if done (case inst
                     :noop (:x state)
                     :addx (+ (:x state) (:arg cmd)))
                   (:x state))

        h (mod (:clock state) 40)
        ]
    {:clock clock
     :program (if done (rest program) program)
     :cycles (if done 0 cycles)
     :x x
     :acc (if (= (mod clock 40) 20) (+ (:acc state) (* clock (:x state))) (:acc state))
     :display (cons (if (<= (- h 1) (:x state) (+ h 1)) \# \.) (:display state))
     })
  )

(defn run [state count]
  (if (> (:clock state) count)
    state
    (recur (tick state) count)))

(defn parse [l]
  (let [tokens (str/split l #"\s+")]
    (case (first tokens)
      "noop" {:inst :noop}
      "addx" {:inst :addx :arg (Integer/parseInt (second tokens))})))

(defn part1 [p]
  (println (:acc (run {:clock 0
                 :program (map parse (u/input-lines p))
                 :cycles 0
                 :x 1
                 :acc 0
                 } 220))))

(defn part2 [p]
  (println (u/parse-text (partition 40 (reverse (:display (run {:clock 0
                                         :program (map parse (u/input-lines p))
                                         :cycles 0
                                         :x 1
                                         :acc 0
                                         } 240)))))))

(defn -main [& args]
  (part1 (first args))
  (part2 (first args)))