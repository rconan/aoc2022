#!/bin/bash
set -euo pipefail

IMAGE=rconan/aoc2022

make .image.txt

exec docker run --rm -i -v "${PWD}:/code" -v "${PWD}/.root:/root" "${IMAGE}" "${@}"
