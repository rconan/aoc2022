(defproject aoc2022 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :global-vars {*warn-on-reflection* true}
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[com.github.clj-easy/graal-build-time "0.1.4"]
                 [instaparse "1.4.12"]
                 [tupelo "21.04.12"]
                 [org.clojure/clojure "1.10.1"]
                 [org.clojure/math.numeric-tower "0.0.5"]
                 [org.clojure/data.priority-map "1.1.0"]
                 [org.clojure/tools.trace "0.7.11"]]
  :repl-options {:init-ns aoc2022.core}
  :main #=(eval (System/getenv "NS"))
  :native-image {:name #=(eval (System/getenv "NS"))
                 :opts ["--verbose"
                        "--report-unsupported-elements-at-runtime"
                        "--no-fallback"]}
  :plugins [[io.taylorwood/lein-native-image "0.3.1"]]
  :profiles {:native-image
             {:jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
