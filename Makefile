.image.txt: .docker/*
	docker build -t rconan/aoc2022 .docker
	docker image inspect -f '{{.Id}}' rconan/aoc2022 > .image.txt

target/aoc2022.%: src/aoc2022/%.clj src/aoc2022/utils.clj project.clj
	NS=aoc2022.$* lein native-image
