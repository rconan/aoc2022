#!/bin/bash
set -euo pipefail
set -x

if [[ -f "src/aoc2022/day${1}.clj" ]]; then
    make "target/aoc2022.day${1}"
    time "target/aoc2022.day${1}" "input/${1}"
fi
